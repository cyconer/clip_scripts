#!/bin/bash
#
# Write the current date to clipboard
# The date is of ISO 8601 form yyyy-MM-dd

date --iso-8601 | tr -d '\n' | xclip -selection c
