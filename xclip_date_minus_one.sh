#!/bin/bash
#
# Inspect the clipboard. If it contains a valid date, reduce it by one day
# If the clipboard does not contain a valid date, leave it untouched
# The date will be in the ISO 8601 form yyyy-MM-dd


DAY=`xclip -o -selection c`
DAY_BEFORE=`date -d "${DAY} -1 days" --iso-8601`

# only apply if it was actually a valid date
# i.e. if the date(1) command performed successfully
if [ "$?" -eq 0 ]
then
    echo "$DAY_BEFORE" | tr -d '\n' | xclip -selection c
fi
